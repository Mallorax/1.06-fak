import {Posts_URL} from "./utils";

const API = {
    Comment_URL: (id: string) => `https://jsonplaceholder.typicode.com/posts/${id}/comments`,
    Post_URL: (id: string) => `https://jsonplaceholder.typicode.com/posts/${id}`,
    User_URL: (id: string) => `https://jsonplaceholder.typicode.com/users/${id}`
};

export interface IPost {
    userId: string;
    id: string;
    title: string;
    body: string;
}

export interface IComment {
    postId: string
    id: string
    name: string
    email: string
    body: string
}

export interface IUser{
    id: string
    name: string
    username: string
    email: string
    address: IAdress
    phone: string
    website: string
    company: ICompany
}

export interface ICompany {
    name: string
    catchPhrase: string
    bs: string

}

export interface IAdress {
    street: string
    suite: string
    city: string
    zipcode: string
    geo: IGeo
}

export interface IGeo {
    lat: string
    lng: string

}

const PostsService = {
    getAllPosts: async () => {
        try {
            const requestResult: IPost[] = await fetch(Posts_URL)
                .then(resp => resp.json())
            return requestResult
        } catch (e) {
            console.log(e)
        }
    },

    getPostById: async (id: string) => {
        try {
            const requestResult: IPost = await fetch(API.Post_URL(id))
                .then(resp => resp.json())
            return requestResult
        }catch (e) {
            console.log(e)
        }
    },


    getComments: async (id: string) => {
        try {
            const resultById: IComment[] = await fetch(API.Comment_URL(id))
                .then(resp => resp.json())
            return resultById
        } catch (e) {
            console.log(e)
        }
    },

    getUserByID: async (id: string) => {
        try {
            const user: IUser = await fetch(API.User_URL(id))
                .then(resp => resp.json())
            return user
        }catch (e) {
            console.log(e)
        }
    }


}
export default PostsService;