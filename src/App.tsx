import React from 'react';
import Home from './views/home/Home';

import {Switch, Route, BrowserRouter} from "react-router-dom";
import PostDetails from "./views/postDetails/PostDetails";
import UserDetails from "./views/userDetails/UserDetails";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path="/user/:id" component={UserDetails}/>
                    <Route path={"/:id"} component={PostDetails}/>
                    <Route path="/" component={Home}/>

                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
