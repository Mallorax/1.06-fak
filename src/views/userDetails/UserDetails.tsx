import React from 'react';
import {useLocation} from "react-router";
import PostsService, {IUser} from "../../services/PostsService";
import {Line} from "../Line";


const UserDetails = () => {
    const [user, setUser] = React.useState<IUser | null>(null)
    const location = useLocation()
    const postArr = location.pathname.split('/')
    const userId = postArr[postArr.length - 1]

    React.useEffect(() => {
        PostsService.getUserByID(userId).then(resp => {
            if (resp) {
                setUser(resp)
            }
        })
    },)

    if (!user) {
        return null
    }

    return (
        <div key={user.id}>
            <h1>Profile of user: {user.name}</h1>
            <Line/>
            <h3>Personal data</h3>
            <div>Username: {user.username}</div>
            <div>Email: {user.email}</div>
            <div>Phone: {user.phone}</div>
            <div>Website: {user.website}</div>
            <Line/>
            <div>
                <h3>Address:</h3>
                <div>Street: {user.address.street}</div>
                <div>Suite: {user.address.suite}</div>
                <div>City: {user.address.city}</div>
                <div>Zipcode: {user.address.zipcode}</div>
                <div>
                    <div>lat: {user.address.geo.lat}</div>
                    <div>lng: {user.address.geo.lng}</div>
                </div>
            </div>
            <Line/>
            <div>
                <h3>Company:</h3>
                <div>Name: {user.company.name}</div>
                <div>Catch phrase: {user.company.catchPhrase}</div>
                <div>Bs: {user.company.bs}</div>
            </div>
            <Line/>
        </div>
    );
};

export default UserDetails;