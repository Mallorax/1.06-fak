import React from 'react';
import PostsService, {IPost} from "../../services/PostsService";
import {Link} from "react-router-dom";
import {Line} from "../Line";



const Home = () => {
    const [posts, setPosts] = React.useState<IPost[] | null>(null)
    React.useEffect(() => {
        PostsService.getAllPosts().then(resp => {
            if (resp) {
                setPosts(resp)
            }
        })
    }, [])

    if (!posts) {
        return null
    }

    return (
        <div>
            {posts.map(post => (
                <div key={post.id}>Title: {post.title}
                    <div>
                        <Link to={`/user/${post.userId}`}>Author: {post.userId}</Link>
                    </div>
                    <div>
                        <Link to={`/${post.id}`}>details</Link>
                    </div>
                    <Line/>
                </div>
            ))}
        </div>);
};

export default Home;