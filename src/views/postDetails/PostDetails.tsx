import React from 'react';
import PostsService, {IComment, IPost} from "../../services/PostsService";
import {useLocation} from "react-router";
import {Link} from "react-router-dom";
import {Line} from "../Line";


const PostDetails = () => {
    const [post, setPost] = React.useState<IPost | null>(null)
    const [comments, setComments] = React.useState<IComment[] | null>(null)
    const location = useLocation()
    const postArr = location.pathname.split('/')
    const postId = postArr[postArr.length - 1]

    React.useEffect(() => {
        PostsService.getPostById(postId).then(resp => {
            console.log(resp)
            if (resp) {
                setPost(resp)
            }
        })
        PostsService.getComments(postId).then(resp => {
            console.log(resp)
            if (resp) {
                setComments(resp)
            }
        })
    }, [])
    console.log(postId)

    if (!post){
        return null
    }
    if (!comments) {
        return (
            <div>
                <div key={post.id}>
                    <h2>{post.title}</h2>
                    <div>{post.body}</div>
                    <Link to={`/user/${post.userId}`}>author: {post.userId}</Link>
                </div>
            </div>
        );
    }
    console.log(postId)

    return (
        <div>
            <div key={post.id}>
                <h2>{post.title}</h2>
                <div>{post.body}</div>
                <Link to={`/user/${post.userId}`}>author: {post.userId}</Link>
            </div>
            <Line/>
            <h3>Comments:</h3>
            {comments.map(comment => (
                <div key={comment.id}>
                    <h5>Comment: {comment.id}</h5>
                    <div>Name: {comment.name}</div>
                    <div>email: {comment.email}</div>
                    <div>{comment.body}</div>
                    <Line/>
                </div>
            ))}
        </div>
    );
};

//sorry api nie zwraca błędu więc powieliłem kod na przypadek bez odpowiedzi

export default PostDetails;