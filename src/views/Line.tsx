import React from "react";

export const Line = () => (
    <hr style={
        {
            color: "black",
            backgroundColor: "black",
            height: 1
        }
    }/>
)